import pymongo
import shlex, subprocess
import collections

def menu_principal():
    print("|----------------------|")
    print("|       OPCIONES       |")
    print("|----------------------|")
    print("|1: Insertar paciente  |")
    print("|2: Buscar paciente    |")
    print("|3: Editar paciente    |")
    print("|4: Eliminar paciente  |")
    print("|0: Salir              |")
    print("|----------------------|")
    opcion = input()
    return opcion

##Funcion para insertar paciente
def insertar():

    nombre = input("Ingrese nombre paciente: ")
    while True:
        edad = int(input("Ingrese la edad del paciente: "))
        if edad>int(0) and edad<int(120):
            break
        else:
            print("Edad no valida, Ingrese la edad nuevamente")

    while True:
        print("|----------------------|")
        print("|    GENERO PACIENTE   |")
        print("|----------------------|")
        print("| M: Masculino         |")
        print("| F: Femenino          |")
        print("| O: Otro              |")
        print("| I: Indefinido        |")
        print("|----------------------|")
        genero = input("Ingrese Genero del paciente: ")
        if genero=="M":
            genero = "Masculino"
            break
        elif genero=="F":
            genero = "Femenino"
            break
        elif genero=="O":
            genero = "Otro"
            break
        elif genero=="I":
            genero = "Indefinido"
            break
        else:
            print("Genero no valido, Ingrese genero nuevamente")

    motivo = input("Ingrese motivo de ingreso: ")

    while True:
        print("|----------------------|")
        print("|   CONDUCTA PACIENTE  |")
        print("|----------------------|")
        print("| 1: Tranquilo         |")
        print("| 2: Agresivo          |")
        print("| 3: Inquieto          |")
        print("| 4: Agitado           |")
        print("| 5: Confuso           |")
        print("| 6: Desorientado      |")
        print("|----------------------|")
        conducta = input("Ingrese la conducta del paciente: ")

        if conducta=="1":
            conducta = "Tranquilo"
            break
        elif conducta=="2":
            conducta = "Agresivo"
            break
        elif conducta=="3":
            conducta = "Inquieto"
            break
        elif conducta=="4":
            conducta = "Agitado"
            break
        elif conducta=="5":
            conducta = "Confuso"
            break
        elif conducta=="6":
            conducta = "Desorientado"
            break
        else:
            print("Opcion no valida, Ingrese nuevamente")

    ingreso = {
        'Nombre_paciente': nombre,
        'Edad_paciente': edad,
        'Genero_paciente': genero,
        'Motivo_consulta': motivo,
        'Conducta_paciente': conducta
    }
    return ingreso

##Funcion buscar paciente
def buscar_paciente():
    busqueda = input("Ingrese nombre del paciente: ")

    paciente = {'Nombre_paciente': busqueda}

    return paciente

##Funcion determinar al paciente que se quiere buscar
def condicion_act():
    nombre_paciente = input("Ingrese el nombre del paciente que quiere actualizar su información: ")
    condicion = {'Nombre_paciente': nombre_paciente}
    return condicion

##Funcion actualizar dato de paciente
def actualizar():

    print("|--------------------------------|")
    print("|¿Que información quiere cambiar?|")
    print("|--------------------------------|")
    print("| 1: Nombre                      |")
    print("| 2: Edad                        |")
    print("| 3: Genero                      |")
    print("| 4: Motivo de consulta          |")
    print("| 5: Conducta                    |")
    print("|--------------------------------|")
    actualizar = input("")


    if actualizar == "1":
        nombre_nuevo = input("Nombre del paciente: ")
        nuevo = {"$set": {'Nombre_paciente': nombre_nuevo}}
        return nuevo

    elif actualizar == "2":
        while True:
            edad_nueva = int(input("Ingrese la edad del paciente: "))
            if edad_nueva>int(0) and edad_nueva<int(120):
                nuevo = {"$set": {'Edad_paciente': edad_nueva}}
                break
            else:
                print("Edad no valida, Ingrese la edad nuevamente")
        return nuevo

    elif actualizar == "3":
        while True:
            print("|----------------------|")
            print("|    GENERO PACIENTE   |")
            print("|----------------------|")
            print("| M: Masculino         |")
            print("| F: Femenino          |")
            print("| O: Otro              |")
            print("| I: Indefinido        |")
            print("|----------------------|")
            genero_nuevo = input("Ingrese Genero del paciente: ")
            if genero_nuevo=="M":
                genero = "Masculino"
                break
            elif genero_nuevo=="F":
                genero = "Femenino"
                break
            elif genero_nuevo=="O":
                genero = "Otro"
                break
            elif genero_nuevo=="I":
                genero = "Indefinido"
                break
            else:
                print("Genero no valido, Ingrese genero nuevamente")

        nuevo = {"$set": {'Genero_paciente': genero_nuevo}}
        return nuevo

    elif actualizar == "4":
        motivo_nuevo = input("Motivo de consulta del paciente: ")
        nuevo = {"$set": {'Motivo_consulta': motivo_nuevo}}
        return nuevo

    elif actualizar == "5":
        while True:
            print("|----------------------|")
            print("|   CONDUCTA PACIENTE  |")
            print("|----------------------|")
            print("| 1: Tranquilo         |")
            print("| 2: Agresivo          |")
            print("| 3: Inquieto          |")
            print("| 4: Agitado           |")
            print("| 5: Confuso           |")
            print("| 6: Desorientado      |")
            print("|----------------------|")
            conducta_nuevo = input("Ingrese la conducta del paciente: ")

            if conducta_nuevo=="1":
                conducta_nuevo = "Tranquilo"
                break
            elif conducta_nuevo=="2":
                conducta_nuevo = "Agresivo"
                break
            elif conducta_nuevo=="3":
                conducta_nuevo = "Inquieto"
                break
            elif conducta_nuevo=="4":
                conducta = "Agitado"
                break
            elif conducta_nuevo=="5":
                conducta_nuevo = "Confuso"
                break
            elif conducta_nuevo=="6":
                conducta_nuevo = "Desorientado"
                break
            else:
                print("Opcion no valida, Ingrese nuevamente")

        nuevo = {"$set": {'Conducta_paciente': conducta_nuevo}}
        return nuevo

    else:
        return False

##Funcion eliminar paciente
def eliminar():
    nombre_eliminar = input("Ingrese nombre del paciente que quiere borrar: ")

    eliminar = {'Nombre_paciente': nombre_eliminar}
    return eliminar

def titulo():
    print ("")
    print ("|----------------------------------------|")
    print ("|        SERVICIO DE URGENCIAS           |")
    print ("|----------------------------------------|")
    print ("")

#Funcion principal
def main():

    subprocess.call(shlex.split('clear'))
    ##Conexión
    client = pymongo.MongoClient("localhost", 27017)
    database = client["urgencia_db"]
    urgencia = database["pacientes"]

    while True:
        titulo()
        eleccion = menu_principal()
        print(eleccion)
        ##Inserción
        if eleccion=="1":
            insercion = urgencia.insert_one(insertar())
        print ("")
        print ("|----------------------------------------|")
        print ("|     INSERCIÓN REALIZADA CON EXITO      |")
        print ("|----------------------------------------|")
        print ("")
        ##Buscar
        elif eleccion=="2":
            print("|----------------------|")
            print("|      PACIENTES       |")
            print("|----------------------|")
            for i in urgencia.find({},{"_id": 0, "Nombre_paciente": 1}):
                print(i)
            busqueda_paciente = urgencia.find(buscar_paciente())
            for j in busqueda_paciente:
                print(j)
        ##Edición
        elif eleccion=="3":
            print("|----------------------|")
            print("|      PACIENTES       |")
            print("|----------------------|")
            for i in urgencia.find({},{"_id": 0, "Nombre_paciente": 1}):
                print(i)
            editar_paciente = urgencia.update_one(condicion_act(), actualizar())
            print ("")
            print ("|----------------------------------------|")
            print ("|   ACTUALIZACIÓN REALIZADA CON EXITO    |")
            print ("|----------------------------------------|")
            print ("")
        ##Eliminar
        elif eleccion=="4":
            for i in urgencia.find({},{"_id": 0, "Nombre_paciente": 1}):
                print(i)
            borrar_paciente = urgenica.delete_one(eliminar())
            print ("")
            print ("|----------------------------------------|")
            print ("|    ELIMINACIÓN REALIZADA CON EXITO     |")
            print ("|----------------------------------------|")
            print ("")
        ##Salir
        elif eleccion=="0":
            quit()
            break
        ##Opción invalida
        else:
            print("Opción no Valida")


    sleep(1)

main()
