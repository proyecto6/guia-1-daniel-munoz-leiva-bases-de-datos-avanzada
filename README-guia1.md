# Guia 1 Bases de datos avanzado - Daniel Muñoz

# REQUERIMIENTOS DEL SISTEMA.

	1) Sistema Operativo Linux
	2) Sistema de gestion de bases de datos Mongodb
	3) Python3
	4) Pymongo

# Funcionamiento
	
 El script fue desarrollado en python3 y en sistema operativo Ubuntu. Para su ejecución:

	En directorio dónde se encuentra script 'guia1.py':
	$ python3 guia1.py

# Paquetes Entorno

	pip (9.0.1)
	pkg-resources (0.0.0)
	pymongo (4.1.1)
	setuptools (39.0.1)
